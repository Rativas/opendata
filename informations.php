<!DOCTYPE html>
<html>
<head>
    <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="Style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="Script.js" charset="utf-8"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
     <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>

	<title>FindYourSup'</title>
</head>
<header>
<h1 id="title">FindYourSup'</h1>
<nav>
    <ul class="nav__links">
        <li><a href="index.php">Accueil</a></li>
    </ul>
</nav>
</header>
<body>
  <div class="info_bigDiv">
  <div class='infos'>
    <?php
      echo "<h3 id='titre_info'>";
      echo ($_GET['libEtab']);
      echo "</h3>";
      echo "<h3>Informations générales</h3>";
      echo"<li>";
          echo"<B>Academie : </B>";
          print($_GET['academie']);
      echo"</li>";
      echo "<li>";
          echo "<B>Type de diplome : </B>";
          print($_GET['diplome']);
      echo "</li>";
      echo"<li>";
          echo"<B>Secteur : </B>";
          print($_GET['sectDiscip']);
      echo"</li>";
      echo "<br>";
      echo "<hr>";
      echo "<h3>Informations sur les étudiants</h3>";
      echo"<li>";
          echo"<B>Nombre d'étudiants inscrits l'année derniere : </B>";
          print($_GET['effectif']);
      echo"</li>";
      echo"<li>";
          echo "<B>Nombres d'Hommes et de Femmes : </B>";
          if ($_GET['hommes'] == "Donnée non disponible") {
            if ($_GET['femmes'] == "Donnée non disponible") {
              print("Données non disponibles");
            }
          }
          else {
            print($_GET['hommes']);
            echo " hommes et ";
            print ($_GET['femmes']);
            echo " femmes";
          }

      echo "</li>";
      echo "<br>";
      echo "<hr>";
      echo "<h3>Informations sur l'établissement</h3>";
      echo "<li>";
          echo "<B>Site web : </B>";
          $site = $_GET['site'];
          echo "<a href='$site'>";
          print($site);
          echo "</a>";
      echo "</li>";
      echo "<li>";
          echo "<B>Adresse : </B>";
          print($_GET['adresse']);
      echo "</li>";

    ?>
  </div>
  <div id="mapid_2" align="center">
    <script>
        var mymap = L.map('mapid_2').setView([46.227638, 2.213749], 5);
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Carte des établissements',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
        }).addTo(mymap);
    </script>
<?php
    $coord1 = $_GET['coord1'];
    $coord2 = $_GET['coord2'];
    $popup = $_GET['popup'];
    echo "<script>";
      echo "var marker = L.marker([".$coord1.",".$coord2."]).bindPopup('$popup').openPopup().addTo(mymap)";
    echo "</script>";
?>
  </div>
</div>
</body>
</html>
