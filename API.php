<?php
class API
{
  function buildFacets($url)
  {
    $contents = file_get_contents($url);
    $results = json_decode($contents, true);
    foreach ($results["facet_groups"][0]["facets"] as $value) {
        echo "<option value='".$value["name"]."'>";
        print($value["name"]);
        echo "</option>";
    }
  }

  static function buildResults($url)
  {

    $contents= file_get_contents($url);
    $results = json_decode($contents, true);


    foreach ($results["records"] as $value) {
        $codeEtab = $value["fields"]["etablissement"];
        $urlSite = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&facet=uai&facet=type_d_etablissement&facet=com_nom&facet=dep_nom&facet=aca_nom&facet=reg_nom&facet=pays_etranger_acheminement&refine.uai=".$codeEtab;
        $json = file_get_contents($urlSite);

        $codeuaiSite = json_decode($json,true);
        $site = $codeuaiSite["records"][0]["fields"]["url"];
        $adresse = $codeuaiSite["records"][0]["fields"]["adresse_uai"];

        $urlMap = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&facet=uai&facet=type_d_etablissement&facet=com_nom&facet=dep_nom&facet=aca_nom&facet=reg_nom&facet=pays_etranger_acheminement&refine.uai=".$codeEtab;
        $jsonMap = file_get_contents($urlMap);
        $codeuai = json_decode($json,true);

        $coord1 = $codeuai["records"][0]["fields"]["coordonnees"][0];
        $coord2 = $codeuai["records"][0]["fields"]["coordonnees"][1];
        $popup = $value["fields"]["etablissement_lib"]." - ".$value["fields"]["reg_ins_lib"];
        echo "<script>";
        echo "var marker = L.marker([".$coord1.",".$coord2."]).bindPopup('$popup').openPopup().addTo(mymap)";
        echo "</script>";

        echo"<div class='resultat'>";
            echo "<div class='infos'>";
            echo"<h3><u><a href='$site'>";
                echo"Etablissement et région : ";
                $libEtab = $value["fields"]["etablissement_lib"];
                print($libEtab);
                echo" - ";
                $regEtab = $value["fields"]["reg_ins_lib"];
                print($regEtab);
            echo"</h3></u></a>";
        echo"<h4>";
           $diplome = $value["fields"]["diplome_rgp"];
           echo($diplome);
        echo"</h4>";
        echo"<li>";
            echo"<B>Academie : </B>";
            $academie = $value["fields"]["aca_etab_lib"];
            print($academie);
        echo"</li>";
        echo"<li>";
            echo"<B>Secteur : </B>";
            $sectDiscip = $value["fields"]["sect_disciplinaire_lib"];
            print($sectDiscip);
        echo"</li>";
        echo"<li>";
            echo"<B>Nombre d'étudiants inscrits l'année derniere : </B>";
            $effectif = $value["fields"]["effectif"];
            print($effectif);
            echo "<br>";
        echo"</li>";
        echo"</div>";
        echo"<form action='";
        $libEtab = urlencode($libEtab);
        $regEtab = urlencode($regEtab);
        $diplome = urlencode($diplome);
        $academie = urlencode($academie);
        $sectDiscip = urlencode($sectDiscip);
        $effectif = urlencode($effectif);
        if(!isset($value["fields"]["hommes"])) {
          $hommes = "Donnée non disponible";
        }
        else if(isset($value["fields"]["hommes"])) {
          $hommes = urlencode($value["fields"]["hommes"]);
        }
        if(!isset($value["fields"]["femmes"]))
        {
          $femmes = "Donnée non disponible";
        }
        else if (isset($value["fields"]["femmes"])) {
          $femmes = urlencode($value["fields"]["femmes"]);
        }
        $site = urlencode($site);
        $adresse = urlencode($adresse);
        $coord1 = urlencode($coord1);
        $coord2 = urlencode($coord2);
        $popup = urlencode($popup);
        echo "informations.php?libEtab=$libEtab&regEtab=$regEtab&diplome=$diplome";
        echo "&academie=$academie&sectDiscip=$sectDiscip&effectif=$effectif";
        echo "&hommes=$hommes&femmes=$femmes&site=$site&adresse=$adresse";
        echo "&coord1=$coord1&coord2=$coord2&popup=$popup";
        echo "' method='post'>";
        echo "<input type='submit' value='Informations' class='input' name='submit'/>";
        echo"</form>";
        echo"</div>";







    }
  }
}
?>
