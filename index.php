<!DOCTYPE html>
<html>

<head>
    <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="Style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="Script.js" charset="utf-8"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
     <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>

	<title>FindYourSup'</title>
</head>

<body>

<header>
<h1 id="title">FindYourSup'</h1>
<nav>
    <ul class="nav__links">
        <li><a href="index.php">Accueil</a></li>
    </ul>
</nav>
</header>
<?php
require ('API.php');
 ?>
<div class="recherche" align="center">
    <label id="label-recherche">Recherche par critères : </label>
<form  method="post">
    <label for="diplome" class="label-select">Par diplome</label>
    <select id="diplome" class="select-recherche" name="Diplome">
    <option value="none" selected disabled hidden>Diplome</option>
    <?php
        $url="https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1&facet=diplome_rgp&refine.rentree_lib=2017-18";
        API::buildFacets($url);

    ?>
    </select>
    <label for="secteur" class="label-select">Par secteur</label>
    <select id="secteur" class="select-recherche" name="Secteur">
    <option value="none" selected disabled hidden>Secteur</option>
    <?php
        $url="https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1&facet=sect_disciplinaire_lib&refine.rentree_lib=2017-18";
        API::buildFacets($url);

    ?>
    </select>

    <label for="region" class="label-select">Par région</label>
    <select id="region" class="select-recherche" name="Région">
      <option value="none" selected disabled hidden>Région</option>
    <?php
        $url="https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1&facet=reg_ins_lib&refine.rentree_lib=2017-18";
        API::buildFacets($url);


    ?>
    </select>

    <input type="submit" value="Rechercher" class="input" name="submit"/>

    </form>
</div>

    <div id="mapid" align="center">
      <script>
          var mymap = L.map('mapid').setView([46.227638, 2.213749], 5);
          L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
          attribution: 'Carte des établissements',
          maxZoom: 18,
          id: 'mapbox/streets-v11',
          accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
          }).addTo(mymap);
      </script>
    </div>


    <?php
    $dip = "";
    $sect = "";
    $reg = "";

    if (!empty($_POST["submit"])) {
      if(isset($_POST['Diplome'])) {
        $dip="&refine.diplome_rgp=".$_POST["Diplome"];
      }
      if(isset($_POST['Secteur'])) {
        $sect="&refine.sect_disciplinaire_lib=".$_POST['Secteur'];
      }
      if(isset($_POST['Région'])) {
        $reg="&refine.reg_ins_lib=".$_POST['Région'];
      }

    $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&facet=rentree_lib&facet=etablissement_type2&facet=etablissement_type_lib&facet=etablissement&facet=etablissement_lib&facet=champ_statistique&facet=dn_de_lib&facet=cursus_lmd_lib&facet=diplome_rgp&facet=diplome_lib&facet=typ_diplome_lib&facet=diplom&facet=niveau_lib&facet=disciplines_selection&facet=gd_disciscipline_lib&facet=discipline_lib&facet=sect_disciplinaire_lib&facet=spec_dut_lib&facet=localisation_ins&facet=com_etab&facet=com_etab_lib&facet=uucr_etab&facet=uucr_etab_lib&facet=dep_etab&facet=dep_etab_lib&facet=aca_etab&facet=aca_etab_lib&facet=reg_etab&facet=reg_etab_lib&facet=com_ins&facet=com_ins_lib&facet=uucr_ins&facet=dep_ins&facet=dep_ins_lib&facet=aca_ins&facet=aca_ins_lib&facet=reg_ins&facet=reg_ins_lib&refine.rentree_lib=2017-18".$dip.$sect.$reg."&rows=30";

    API::buildResults($url);
    }
    ?>

</body>
<footer>
  <a href="https://bitbucket.org/Rativas/opendata/src/master/">
    Lien vers le dépôt Bitbucket </a>
</footer
</html>
